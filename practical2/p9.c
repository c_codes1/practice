//take an input from the user and print the number in reverse.
//input:12345
//output:54321


#include<stdio.h>
void main(){
	int x,reminder;
	printf("enter a number\n");
	scanf("%d",&x);
	
	while(x!=0){
		reminder=x%10;
		printf("%d",reminder);
		x=x/10;
	}
	printf("\n");
	
}
