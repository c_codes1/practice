/* if possible take numbers of rows from the user :
                                                   1 2 3
						   a b c
						   1 2 3
						   a b c   */
#include<stdio.h>
void main(){
	int b;
        printf("Enter a rows:\n");
	scanf("%d",&b);

	for(int i=1; i<=b;i++){
                int a=1;
		int ch='a';
		for(int j=1; j<=b; j++){
			if(i%2==0){
				printf("%c ",ch);
				ch++;
			}else{
				printf("%d ",a);
				a++;
			}
		}
		printf("\n");
	}
}
