// memory on heap section (malloc)


#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct employee{
	int empId;
	char empName[20];
	float empSal;
	struct employee *next;
}emp;

void accessData(emp *ptr){
	printf("%d\n",ptr->empId);
	printf("%s\n",ptr->empName);
	printf("%f\n",ptr->empSal);
	printf("%p\n",ptr->next);
}

void main(){

	emp *Emp1=(emp*)malloc(sizeof(emp));
	emp *Emp2=(emp*)malloc(sizeof(emp));
	emp *Emp3=(emp*)malloc(sizeof(emp));

	Emp1->empId=1;
	strcpy(Emp1->empName,"TEJAS");
	Emp1->empSal=60.0;
	Emp1->next=Emp2;

	Emp1->next->empId=2;
        strcpy(Emp1->next->empName,"Sahil");
        Emp1->next->empSal=65.0;
        Emp1->next->next=Emp3;

	Emp1->next->next->empId=3;
        strcpy(Emp1->next->next->empName,"Atharv");
        Emp1->next->next->empSal=66.0;
        Emp1->next->next->next=NULL;

	accessData(Emp1);
	accessData(Emp2);
	accessData(Emp3);

}



	


