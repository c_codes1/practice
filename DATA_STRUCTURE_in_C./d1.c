// self Referencial structure:


#include<stdio.h>
#include<string.h>

typedef struct Employee{

	int empId;
	char empName[20];
	float sal;
	struct Employee *next;
}emp;

void main(){

	emp obj1,obj2,obj3;

	obj1.empId=1;
	strcpy(obj1.empName,"Kanha");
	obj1.sal=50.00;
	obj1.next=&obj2;

	obj2.empId=2;
	strcpy(obj2.empName,"Rahul");
	obj2.sal=60.00;
	obj2.next=&obj3;

	obj3.empId=3;
        strcpy(obj3.empName,"Ashish");
	obj3.sal=65.00;
	obj3.next=NULL;

	//by using Address:
	
        emp *head=&obj1;

	head-> empId=1;
	strcpy(head-> empName,"Kanha");
	head->sal=50.00;
	head->next=&obj2;

	head->next->empId=2;
	strcpy(head->next->empName,"Rahul");
	head->next->sal=60.00;
	head->next->next=&obj3;

	head->next->next->empId=3;
	strcpy(head->next->next->empName,"Ashish");
	head->next->next->next=NULL;
}
