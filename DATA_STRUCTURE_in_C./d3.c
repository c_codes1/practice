#include<stdio.h>
#include<string.h>


struct company{

	int empcount;
	char compName[20];
	float Rev;
	struct company *next;
};

void main(){

	struct company obj1,obj2,obj3;

	struct company *ptr=&obj1;

	ptr->empcount=50000;
	strcpy(ptr->compName,"AMEZON");
	ptr->Rev=15000.00;
	ptr->next=&obj2;

	ptr->next->empcount=60000;
        strcpy(ptr->next->compName,"GOOGLE");
        ptr->next->Rev=50000.00;
        ptr->next->next=&obj3;

	ptr->next->next->empcount=650000;
        strcpy(ptr->next->next->compName,"APPLE");
        ptr->next->next->Rev=650000.00;
        ptr->next->next->next=NULL;

	printf("%d\n",ptr->empcount);
	printf("%s\n",ptr->compName);
	printf("%f\n",ptr->Rev);

        printf("%d\n",ptr->next->empcount);
        printf("%s\n",ptr->next->compName);
        printf("%f\n",ptr->next->Rev);

	printf("%d\n",ptr->next->next->empcount);
        printf("%s\n",ptr->next->next->compName);
        printf("%f\n",ptr->next->next->Rev);
}








