#include<stdio.h>
#include<stdlib.h>

typedef struct student{
	int id;
	float ht;
	struct student *next;
}stud;

stud *head=NULL;

stud addnode(){
	stud *newnode=(stud*)malloc(sizeof(stud));
	newnode->id=1;
	newnode->ht=5.5;
	newnode->next=NULL;

	head=newnode;
}
void printLL(){
	stud *temp=head;
	while(temp!=NULL){
		printf("%d\n",temp->id);
		printf("%f\n",temp->ht);
		temp=temp->next;
	}
}
void main(){
	addnode();
	printLL();
}
