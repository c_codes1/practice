

#include<stdio.h>
#include<stdlib.h>


typedef struct movie{
	char mName[20];
	int tickets;
	float imdb;
	struct movie *next;
}movie;

movie *head=NULL;

void addNode(){
	getchar();
	movie *newnode=(movie*)malloc(sizeof(movie));
	printf("Enter a name of movie:\n");
//	scanf("%[^\n]",newnode->mName); correct way its also working:
//	gets("%s",newnode->mName);   Riskey(ERROR can be occur:)
//      fgets(newnode->mName,20,stdin); // "\n"	problem


	// CORRECT WAY:
	int ch;
	int i=0;
	while((ch=getchar())!='\n'){
		(*newnode).mName[i]=ch;
		i++;
	}
	printf("Enter no. of tickets:\n");
	scanf("%d",&newnode->tickets);
	printf("Enter IMDB rating:\n");
	scanf("%f",&newnode->imdb);

	newnode->next=NULL;

	if(head==NULL){
		head=newnode;
	}else{
		movie *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newnode;
	}
}

void printLL(){
	movie *temp=head;
	while(temp!=NULL){
		printf("|%s|",temp->mName);
		printf("tickets->%d",temp->tickets);
		printf("|IMDB->%2f",temp->imdb);
		printf("\n");

		temp=temp->next;
	}
}


void main(){

	int nodes;
	printf("enter the no. of nodes to print:\n");
	scanf("%d",&nodes);

	for(int i=1; i<=nodes;i++){
		addNode();
	}

	printLL();


}
