// Q-1] WAP for the link-list of malls consisting of its name, number of shopes,& revenue; connect 3 malls in the Linked-list & print their data:


#include<stdio.h>
#include<stdlib.h>

typedef struct malls{
	char name[20];
	int shopN;
	float rev;
	struct malls *next;
}mall;

mall *head=NULL;

void addNode(){
	mall *newnode=(mall*)malloc(sizeof(mall));
	printf("Enter name of mall:\n");
	//fgets(newnode->name,15,stdin);
	scanf("%[^\n]",newnode->name);
	printf("enter shop numbers in mall:\n");
	scanf("%d",&newnode->shopN);
	printf("enter revanue:\n");
	scanf("%f",&newnode->rev);
	getchar();

	newnode->next=NULL;

	if(head==NULL){
		head=newnode;
	}else{
		mall *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newnode;
	}
}

void printLL(){

	mall *temp=head;

	while(temp!=NULL){
		printf("%s :",temp->name);
		printf("%d :",temp->shopN);
		printf("%f :",temp->rev);
		printf("\n");
		temp=temp->next;
	}
}
void main(){

	addNode();
	addNode();
	addNode();
	printLL();
}
