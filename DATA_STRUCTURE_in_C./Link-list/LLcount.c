//addData(),printLL(),count();


#include<stdio.h>
#include<stdlib.h>


typedef struct Employee{
	char name[20];
	int id;
	float sal;
	struct Employee *next;
}emp;

emp *head=NULL;

void addNode(){
	getchar();
	emp *newnode=(emp*)malloc(sizeof(emp));
	printf("enter a name of employee:\n");
	char ch;
	int i=0;
	while((ch=getchar())!='\n'){
		(*newnode).name[i]=ch;
		i++;
	}
	printf("Enter a id no of employee:\n");
	scanf("%d",&newnode->id);
	printf("enter a salary of employee:\n");
	scanf("%f",&newnode->sal);

	newnode->next=NULL;
	if(head==NULL){
		head=newnode;
	}else{
		emp *temp= head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newnode;
	}
}

void count(){

	emp *temp=head;
	int counter=0;
	while(temp!=NULL){
		temp=temp->next;
		counter++;
	}
	printf("count of nodes:%d\n",counter);
}

int printLL(){
	emp *temp=head;
	int count=0;
	while(temp!=NULL){
		printf("[%s|",temp->name);
		printf("ID:%d|",temp->id);
		printf("sal:%f]",temp->sal);
		temp=temp->next;
		count++;
	}
	return count;

}

void main(){
	int node;
	printf("Enter a no of nodes to create a nodes:\n");
	scanf("%d",&node);
	for(int i=1; i<=node; i++){
		addNode();
	}
	count();
	int count=printLL();
	printf("\ncount:%d",count);
}

		

