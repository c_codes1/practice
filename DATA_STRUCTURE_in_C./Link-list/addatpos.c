#include<stdio.h>
#include<stdlib.h>


typedef struct Employee{
	char name[20];
	int id;
	float sal;
	struct Employee *next;
	
}emp;

emp *head=NULL;

emp * createNode(){
	emp *newnode=(emp*)malloc(sizeof(emp));
	printf("enter the name of employee:\n");
	getchar();
	char ch;
	int i=0;
	while((ch=getchar())!='\n'){
		(*newnode).name[i]=ch;
		i++;
	}
	printf("enter a employee id :\n");
	scanf("%d",&newnode->id);
	getchar();
	printf("enter the employee salary:\n");
	scanf("%f",&newnode->sal);

	newnode->next=NULL;
	return newnode;
}

emp* addNode(){
	emp *newnode=createNode();

	if(head==NULL){
		head=newnode;
	}else{
		emp *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newnode;
	}
	return newnode;

}

void addFirst(){
	emp *newnode=createNode();

	if(head==NULL){
		head=newnode;
	}else{
		newnode->next=head;
		head=newnode;
	}
}

void addatpos(int pos){
	emp *newnode=createNode();
	emp *temp=head;
	while(pos-2){
		temp=temp->next;
		pos--;
	}
	newnode->next=temp->next;
	temp->next=newnode;
}
void addLast(){
	emp *newnode=addNode();
}

void printLL(){
	emp *temp=head;
	while(temp!=NULL){
		printf("%s:",temp->name);
		printf("%d",temp->id);
		printf("%f\n",temp->sal);
		temp=temp->next;
	}
}
void main(){
	addNode();
	addNode();
	addFirst();
	int possition;
	printf("enter a possition where you want to add node:\n");
	scanf("%d",&possition);
	getchar();
	addatpos(possition);
	addLast();
	printLL();
}
