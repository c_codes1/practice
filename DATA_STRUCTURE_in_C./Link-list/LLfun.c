#include<stdio.h>
#include<stdlib.h>

typedef struct employee{
	char name[20];
	int id;
	float sal;
	struct employee *next;
}emp;

emp *head=NULL;

emp* createNode(){
	getchar();
	emp *newnode=(emp*)malloc(sizeof(emp));
	printf("ENter a name of employee:\n");
	char ch;
	int i=0;
	while((ch=getchar())!='\n'){
		(*newnode).name[i]=ch;
		i++;
	}
	printf("enter a employee id no:\n");
	scanf("%d",&newnode->id);
	getchar();
	printf("enter a salary of employee:\n");
	scanf("%f",&newnode->sal);

	newnode->next=NULL;
	return newnode;
}
emp* addNode(){
	emp *newnode=createNode();

	if(head==NULL){
		head=newnode;
	}else{
		emp *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newnode;
	}
	return newnode;
}
void addFirst(){
	emp *newnode=createNode();

	if(head==NULL){
		head=newnode;
	}else{
		newnode->next=head;
		head=newnode;
	}
}
int nodeCount(){
	emp *temp=head;
	int count=0;
	while(temp!=NULL){
		count++;
		temp=temp->next;
	}
	printf("count of nodes is:%d\n",count);
	return count;
}

int addatpos(int pos){
	int count=nodeCount();
	
	if(pos<=0 || pos>=count+2){
		printf("invalid node position:\n");
		return -1;
	}else{
		if(pos==count+1){
			addNode();
		}else if(pos==1){
			addFirst();
		}else{
			emp *newnode=createNode();
			emp *temp=head;

			while(pos-2){
				temp=temp->next;
				pos--;
			}
			newnode->next=temp->next;
			temp->next=newnode;
			return 0;
		}
	}
}

void addLast(){
	emp *newnode=addNode();
}
void delFirst(){
	emp *temp=head;
	head=temp->next;
	free(temp);
}
void printLL(){
	emp *temp=head;
	while(temp!=NULL){
		printf("empName:%s\n",temp->name);
		printf("empId:%d\n",temp->id);
		printf("empsal:%f\n",temp->sal);
		temp=temp->next;
	}
}

void main(){

	char choice;

	do{
		printf("1:addNode\n");
		printf("2:addFirst\n");
		printf("3:addlast\n");
		printf("4:nodeCount\n");
		printf("5:addatpos\n");
		printf("6:delatfirst\n");
		printf("7:printLL\n");
		int ch;
		printf("\nEnter choice:\n");
		scanf("%d",&ch);
		
		switch(ch){
			case 1:
				addNode();
				break;
		        case 2:
				addFirst();
				break;

			case 3:	
				addLast();
				break;
			case 4:
				nodeCount();
				break;
			case 5:{
				int pos;
				printf("Enter a number\n");
				scanf("%d",&pos);
				addatpos(pos);
			       } 
			       break;
                        
			case 6:
			         delFirst();
			         break;
			case 7:
			         printLL();
			         break;
			
			default:
				 printf("Wrong choice:\n");

		}
		getchar();
		printf("Do you want to continue:\n");
		scanf("%c",&choice);
	}
	while(choice=='y'||choice=='Y');
	
}


