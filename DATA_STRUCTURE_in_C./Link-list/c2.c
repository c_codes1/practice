
#include<stdio.h>
#include<stdlib.h>

typedef struct student{

	int id;
	float ht;
	struct student *next;
}stud;

void main(){

 stud *head=NULL;

 stud *newnode=(stud *)malloc(sizeof(stud));
 newnode->id=1;
 newnode->ht=5.5;
 (*newnode).next=NULL;

 head=newnode;

 newnode=(stud*)malloc(sizeof(stud));
 newnode->id=2;
 newnode->ht=6.5;
 newnode->next=NULL;

 head->next=newnode;

 newnode=(stud*)malloc(sizeof(stud));
 newnode->id=3;
 newnode->ht=7.5;
 newnode->next=NULL;
 head->next->next=newnode;

 stud *temp=NULL;
 temp=head;

 while(temp!=NULL){
	printf("%d\n",temp->id);
	printf("%f\n",temp->ht);

	temp=temp->next;
 }

}

