// user input:

#include<stdio.h>
#include<stdlib.h>

typedef struct movie{

	char mName[20];
	float imdb;
	struct movie *next;
}mv;

mv *head=NULL;
void addData(){

	mv *newnode=(mv*)malloc(sizeof(mv));
	
	printf("Enter a name of movie:\n");
	fgets(newnode->mName,15,stdin);
        
	printf("Enter a Rating:\n");
	scanf("%f",&newnode->imdb);
	
	getchar();

	newnode->next=NULL;

	if(head==NULL){
		head=newnode;
	}else{
		mv *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newnode;
	}
}
void printLL(){

	mv *temp=head;
        
	while(temp!=NULL){
		printf("%s",temp->mName);
		printf("|%f",temp->imdb);

		temp=temp->next;
	}
}

void main(){

	addData();
	addData();
	addData();
	printLL();



}
