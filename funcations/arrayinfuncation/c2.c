// Passing array in funcation :


#include<stdio.h>

void printArr1(int *ptr,int arrsize){
	for(int i=0; i<arrsize; i++){
		printf("%d\n",*(ptr+i));
	}
}
void printArr2(int arr[],int arrsize){
	for(int i=0; i<arrsize; i++){
		printf("%d\n",arr[i]);
	}
}
void main(){
	int arr[]={10,20,30,40,50};
	int arrsize= sizeof(arr)/sizeof(int);
	printArr1(arr,arrsize);
	printArr2(arr,arrsize);
}
                	
