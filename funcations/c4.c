// swapping

#include<stdio.h>
void main(){

	int x,y;
	printf("Enter the value of x:");
	scanf("%d",&x);
	printf("Enter the value of y:");
	scanf("%d",&y);

	printf("x=%d\n",x);
	printf("y=%d\n",y);

	int temp;
	temp=x;
	x=y;
	y=temp;

	printf("After swapping:\n");

	printf("x=%d\n",x);
	printf("y=%d\n",y);
}
