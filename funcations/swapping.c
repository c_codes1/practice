// Swapping(call by address):


#include<stdio.h>
void swap(int *ptr);
void main(){

	int x;
	printf("Enter the value of x:");
	scanf("%d",&x);

	printf("x=%d\n",x);

	swap(&x);

	printf("x=%d\n",x);
}
void swap(int *ptr){

	printf("Addr's:%p\n",ptr);
	printf("x:%d\n",*ptr);

	*ptr=50;
}
