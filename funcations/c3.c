// Swapping (call by value) : without using third variable.

#include<stdio.h>
void swap(int,int);
void main(){
	
	int x;
	int y;

	printf("Enter the value of x:");
	scanf("%d",&x);
	printf("Enter the value of y:");
	scanf("%d",&y);

	printf("x=%d\n",x);
	printf("y=%d\n",y);

	swap(x,y);
	printf("x=%d\n",x);
	printf("y=%d\n",y);
}
void swap(int x,int y){

        printf("Before swapping.values in Fun:\n");
	printf("x=%d\n",x);
	printf("y=%d\n",y);
	printf("After swapping...\n");

	int temp;
	temp=x;
	x=y;
	y=temp;

	printf("x=%d\n",x);
	printf("y=%d\n",y);
}



