/* WAP to print numbers in a given range and their multiplicative inverse or 
reciprocal in 1/x.
The expected output for range 1-5:
1=1
2=0.5
3=0.33
4=0.25
5=0.20 */

#include<stdio.h>
void main(){

	float val;
	int a,b;
	printf("enter start\n");
	scanf("%d",&a);
	printf("enter end\n");
	scanf("%d",&b);
	for( float i=a; i<=b; i++){
		val=1/i;
		printf("%f = %f\n",i,val);

	}
}
