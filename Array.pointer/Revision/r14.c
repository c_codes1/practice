
#include<stdio.h>
/*void main(){

	int arr1[]={10,20,30};
	int arr2[]={10,20,30};

	if(arr1==arr2){                           //note:array name is address of first element.
		printf("arrays are equal:\n");
	}else{
		printf("arrays are not equal:\n");
	}
}
//output: arrays are not equal.   */


void main(){

	int arr1[4]={10,20,30,40};
	int arr2[4]={10,20,40,50};
	int flag;

	for(int i=0; i<4; i++){
		if(arr1[i]==arr2[i]){
			flag=1;
		}else{
			flag=0;
			break;
		}
	}
	if(flag==1){
		printf("Arrays are equal:\n");
	}else{
		printf("Array are not equal:\n");
	}
}

