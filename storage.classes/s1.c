//register storage classes.

#include<stdio.h>
void main(){
	register int x=10;

	printf("%d\n",x);// 10
//Error	printf("%p\n",&x);

	/*error: address of register variable ‘x’ requested
    8 |         printf("%p\n",&x);//
      |         ^~~~~~
                                                     */
}

// memory - CPU
// applies on only local scope
// default value- garbage
// lifetime - scope
