/* static storage classes- 1. local static
                           2. globel static */

#include<stdio.h>

void fun(){
 // 	int x=10;         o/p :-11 11 11 because it gets storage in stack of fun()
//	static int x=10;  o/p :-11 12 13 because it gets storage in data section
	++x;
	printf("%d\n",x);
}
void main(){

	fun();
	fun();
	fun();
}



