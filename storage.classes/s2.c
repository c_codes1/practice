// auto storage classes

#include<stdio.h>

// auto int x=10; 
 
/*  error: file-scope declaration of ‘x’ specifies ‘auto’
    4 | auto int x=10; */



void main(){

 auto int x=10;
 printf("%p\n",&x);
 printf("%d\n",x);
}

