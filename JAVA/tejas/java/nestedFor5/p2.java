import java.io.*;
class TJ7{


	void fun(int row){

		char ch1='#';
		char ch2='=';

		for(int i=1; i<=row; i++){
			for(int j=1; j<=row; j++){

				if(j==i){
					System.out.print(ch1+" ");
				}else{
					System.out.print(ch2+" ");
				}
			}
			System.out.println();
		}

	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a Number of Rows:");
		int row=Integer.parseInt(br.readLine());

		TJ7 obj=new TJ7();
		obj.fun(row);
	}
}


