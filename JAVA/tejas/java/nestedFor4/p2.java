/*
 
1
3 4
6 7 8
10 11 12 13
15 16 17 18 19  */





import java.util.Scanner;

class TJ7{


	public static void main(String[] arg){

		Scanner obj= new Scanner(System.in);

		System.out.println("Enter a rows number");
		int row=obj.nextInt();

		System.out.println("Enter a starting number:");
		int num=obj.nextInt();

		System.out.print("\n");

		for(int i=1; i<=row; i++){
			for(int j=1; j<=i; j++){

				System.out.print(num++ +" ");
			}
			num++;
			System.out.println();
		}
	}
}
