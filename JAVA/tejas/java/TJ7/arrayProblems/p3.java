//3. Calculate the sum of all elements in an array
import java.io.*;
class addEle {

    void sum(int[]arr){
        int sum=0;
        for(int i=0; i<arr.length;i++){
            sum=sum+arr[i];      
        }
        System.out.println("sum of element is :"+sum);
    }

    public static void main(String[] args)throws IOException {
        
        BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
        System.out.println("enter size of array:");
        int size=Integer.parseInt(br.readLine());
        int arr[]=new int[size];
        System.out.println("Enter array elements:");
        for(int i=0; i<arr.length; i++){
            arr[i]=Integer.parseInt(br.readLine());
        }

        addEle obj=new addEle();
        obj.sum(arr);
        
    }
    
}


