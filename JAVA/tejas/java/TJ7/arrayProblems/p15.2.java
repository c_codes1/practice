//17. Remove duplicate elements from an unsorted array.(by sorting )
import java.io.*;
import java.util.Arrays;

class removeDuplicate{

    void removeD(int[]arr){
        Arrays.sort(arr);
        int j=0;
        int len=arr.length;

        for(int i=0; i<len-1; i++){

            if(arr[i]!=arr[i+1]){
                arr[j++]=arr[i];
            }

        }
        arr[j++]=arr[len-1];
        for(int x: arr){
            System.out.println(x);
        }
        

    }


    public static void main(String[] args)throws IOException {

        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter size of array:");
        int size=Integer.parseInt(br.readLine());
        int[]arr=new int[size];
        System.out.println("Enter a array elements:");
        for(int i=0; i<arr.length;i++){
            arr[i]=Integer.parseInt(br.readLine());
        }
        removeDuplicate obj=new removeDuplicate();
        obj.removeD(arr);
    }
}
