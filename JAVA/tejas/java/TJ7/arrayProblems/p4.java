//4. Calculate the average of all elements in an array.

import java.io.*;
class avgEle {

    void avg(int[]arr){
        int sum=0;
        float avg=0;
        for(int i=0; i<arr.length;i++){
            sum=sum+arr[i];   
            avg=sum/arr.length;   
        }
        System.out.println("average  of element is :"+avg);
    }

    public static void main(String[] args)throws IOException {
        
        BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
        System.out.println("enter size of array:");
        int size=Integer.parseInt(br.readLine());
        int arr[]=new int[size];
        System.out.println("Enter array elements:");
        for(int i=0; i<arr.length; i++){
            arr[i]=Integer.parseInt(br.readLine());
        }

        avgEle obj=new avgEle();
        obj.avg(arr);
        
    }
    
}


