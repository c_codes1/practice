//16. Remove duplicate elements from an unsorted array.(Using HashSet)
import java.util.HashSet;
class removeDuplicate{

    public static void main(String[] args) {
        
        int[]arr=new int[]{10,20,10,30,40,50};

        HashSet<Integer> hs=new HashSet<Integer>();

        for(int i=0; i<arr.length;i++){
            hs.add(arr[i]);
        }

        //System.out.println(" "+hs);
	
	for(int x: hs){

		System.out.println(x);
	}
    
    
    }
}
