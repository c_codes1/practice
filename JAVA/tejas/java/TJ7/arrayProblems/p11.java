//11. Find the index of the first occurrence of an element in an array.

import java.io.*;
class firstOccurance{

    int occurance(int[]arr,int num){

        for(int i=0; i<arr.length;i++){

            if(arr[i]==num){
                return i;
            }

        }
        return -1;
    }


    public static void main(String[] args)throws IOException {

        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter a size of an Array:");
        int size=Integer.parseInt(br.readLine());
        int[]arr=new int[size];
        System.out.println("Enter array elements:");
        for(int i=0; i<arr.length;i++){
            arr[i]=Integer.parseInt(br.readLine());
        }
        firstOccurance obj=new firstOccurance();
        System.out.println("Enter a number to find occurance");
        int num=Integer.parseInt(br.readLine());
        int index=obj.occurance(arr,num);

        if(index==-1){
            System.out.println("number not found!!!!!");
        }else{
            System.out.println("the first occurance of the element "+num+" is: "+index);
        }
    }
}
