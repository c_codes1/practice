//1. Find the maximum element in an array
import java.io.*;
class maxEle {

    void max(int[]arr){
        int max=arr[0];
        for(int i=0; i<arr.length;i++){

            if(max<=arr[i]){
                max=arr[i];
            }
        }
        System.out.println("maximum element is :"+max);
    }

    public static void main(String[] args)throws IOException {
        
        BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
        System.out.println("enter size of array:");
        int size=Integer.parseInt(br.readLine());
        int arr[]=new int[size];
        System.out.println("Enter array elements:");
        for(int i=0; i<arr.length; i++){
            arr[i]=Integer.parseInt(br.readLine());
        }

        maxEle obj=new maxEle();
        obj.max(arr);
        
    }
    
}
