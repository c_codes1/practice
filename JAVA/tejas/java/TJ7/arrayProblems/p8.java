//8. Find the second largest element in an array

import java.io.*;
class sortEle {

    void decending(int[]arr){
        
        for(int i=0; i<arr.length;i++){
            for(int j=i+1; j<arr.length;j++){

                if(arr[i]<arr[j]){
                    int temp=arr[i];
                    arr[i]=arr[j];
                    arr[j]=temp;
                }

            }
        }
        System.out.println("second largest element is:"+arr[1]);
       
    }

    public static void main(String[] args)throws IOException {

        BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
        System.out.println("enter size of array:");
        int size=Integer.parseInt(br.readLine());
        int arr[]=new int[size];
        System.out.println("Enter array elements:");
        for(int i=0; i<arr.length; i++){
            arr[i]=Integer.parseInt(br.readLine());
        }

        sortEle obj=new sortEle();
        obj.decending(arr);

    }

}
