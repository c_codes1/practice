//7. Reverse an array.

import java.io.*;
class reverseArr {

    void reverseARR(int[]arr){
        int start=0;
        int end=arr.length-1;
        while(start<end){

            int temp=arr[start];
            arr[start]=arr[end];
            arr[end]=temp;

            start++;
            end--;

        }
	System.out.println("array is reversed!!!");
	for(int i=0; i<arr.length; i++){

		System.out.println(arr[i]);
	
	}

    }


    public static void main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter size of an array:");
        int size=Integer.parseInt(br.readLine());
        int[]arr=new int[size];
        System.out.println("Enter array elements");
        for(int i=0; i<arr.length; i++){
            arr[i]=Integer.parseInt(br.readLine());
        }       
        reverseArr obj=new reverseArr();
        obj.reverseARR(arr);
    }
    
}
