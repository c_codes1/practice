//12. Find the index of the last occurrence of an element in an array.

import java.io.*;
class lastOccurance{

    int occurance(int[]arr,int num){

        int lastoc=-1;
        for(int i=0; i<arr.length;i++){

            if(arr[i]==num){
               lastoc=i;
            }

        }
        return lastoc;
    }


    public static void main(String[] args)throws IOException {

        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter a size of an Array:");
        int size=Integer.parseInt(br.readLine());
        int[]arr=new int[size];
        System.out.println("Enter array elements:");
        for(int i=0; i<arr.length;i++){
            arr[i]=Integer.parseInt(br.readLine());
        }
        lastOccurance obj=new lastOccurance();
        System.out.println("Enter a number to find occurance");
        int num=Integer.parseInt(br.readLine());
        int index=obj.occurance(arr,num);

        if(index==-1){
            System.out.println("number not found!!!!!");
        }else{
            System.out.println("the last occurance of the element "+num+" is: "+index);
        }
    }
}
