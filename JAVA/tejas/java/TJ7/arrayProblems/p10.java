//10.Find the frequency of an element in an array.


import java.io.*;
class frequency{

    void freq(int []arr){
        for(int i=0; i<arr.length; i++){
            int count=0;
            for(int j=0; j<arr.length; j++){
                if(arr[i]==arr[j]){
                    count++;
                }
            }
            System.out.println("frequency of array element"+arr[i]+"is :"+count);
        }
    }


    public static void main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter size of an array:");
        int size=Integer.parseInt(br.readLine());
        int[]arr=new int[size];
        System.out.println("Enter array elements:");
        for(int i=0; i<arr.length; i++){
            arr[i]=Integer.parseInt(br.readLine());
        }
        frequency obj=new frequency();
        obj.freq(arr);
    }
}
