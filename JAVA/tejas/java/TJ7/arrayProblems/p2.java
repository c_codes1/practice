//2. Find the minimum element in an array.
import java.io.*;
class minEle {

    void min(int[]arr){
        int min=arr[0];
        for(int i=0; i<arr.length;i++){

            if(min>=arr[i]){
                min=arr[i];
            }
        }
        System.out.println("minimum element is :"+min);
    }

    public static void main(String[] args)throws IOException {
        
        BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
        System.out.println("enter size of array:");
        int size=Integer.parseInt(br.readLine());
        int arr[]=new int[size];
        System.out.println("Enter array elements:");
        for(int i=0; i<arr.length; i++){
            arr[i]=Integer.parseInt(br.readLine());
        }

        minEle obj=new minEle();
        obj.min(arr);
        
    }
    
}

