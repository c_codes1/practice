
class ArmstrongNumbers {
    public static void main(String[] args) {
        for (int i = 1; i <= 1000; i++) {
            int num = i;
            int sum = 0;
            int count = 0;

            // Count number of digits in the current number
            while (num > 0) {
                num = num / 10;
                count++;
            }

            // Calculate sum of digits raised to the power of count
            num = i;
            while (num > 0) {
                int digit = num % 10;
                int power = 1;
                for (int j = 0; j < count; j++) {
                    power = power * digit;
                }
                sum = sum + power;
                num = num / 10;
            }

            // Check if the number is an Armstrong number
            if (sum == i) {
                System.out.println(i + " is an Armstrong number");
            }
        }
    }
}

