
class Practice extends Thread {

	public void run() {
	
		for(int i=0; i<10; i++) {
		
			System.out.println("in fun");
			
			try{
				sleep(2000);
			} catch(InterruptedException ie){
			
				System.out.println("Interrupt keli");
			}
		}
	}
}

class Demo {

	public static void main(String[] args) throws InterruptedException {
	
		Practice obj  = new Practice();   // new/born state
		obj.start();
	
		for(int i=0; i<10; i++) {

                        System.out.println("in main");
			Thread.currentThread().sleep(2000);
                }
	}
}
