class myThread implements Runnable{

	public void run(){
	
		System.out.println(Thread.currentThread());	
	}
}
class ThreadDemo{

	public static void main(String[] args){

		myThread obj=new myThread();
		Thread obj1=new Thread(obj);
		obj1.start();
		
		Thread t1 = Thread.currentThread();
		System.out.println(t1.getPriority());

		t1.setPriority(7);

		myThread obj3=new myThread();
		Thread obj4=new Thread(obj3);
		obj4.start();
		
	}

}
