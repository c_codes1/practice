class Demo{

	static{

		System.out.println("in Static block1");
	}

	void fun(){
		System.out.println("in Fun");
	}
	public static void main(String[] args){
	}
}
class Client{

	static{

		System.out.println("in static block2");
	}

	public static void main(String[] args){

		System.out.println("in main");
		Demo obj=new Demo();
	}
	static{

		System.out.println("in static block3");
	}
}
