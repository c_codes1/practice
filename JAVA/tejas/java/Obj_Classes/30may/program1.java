class Player{

	private String pName=null;
	private int jerNo=0;
	String name=new String("virat");
	String InstString="virat";

	Player(int jerNo,String pName){

		System.out.println("when instance staring is null:"+System.identityHashCode(this.pName));
		this.jerNo=jerNo;
		this.pName=pName;
		System.out.println("In constructor");
		System.out.println("after change access in constructor:"+System.identityHashCode(this.pName));
	}
	void info(){
		System.out.println(jerNo+"="+pName);
		System.out.println("after change access from non static method:"+System.identityHashCode(this.pName));
	}

	void fun(String x){

		System.out.println("**********In fun*********");

		String name="virat";
		System.out.println("local string litral:"+System.identityHashCode(name));
		System.out.println("instance string litral:"+System.identityHashCode(InstString));
		System.out.println("instance object string:"+System.identityHashCode(this.name));
		name=x;
		System.out.println("after change in local string litral(virat):"+System.identityHashCode(name));
		System.out.println("after change in instance string litral"+System.identityHashCode(InstString));

	}
}
class Client{

	public static void main(String[] args){
//		Player obj1=new Player(45,"Rohit");
//		obj1.info();

		Player obj2=new Player(18,"virat");
		obj2.info();
		
//		Player obj3=new Player(07,"mahi");
//		obj3.info();
		obj2.fun("virat");
	}
}

