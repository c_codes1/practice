class Demo{

        static int y=20;
	int x=10;
	private String admin="TJ7";

	static{
	//	System.out.println(admin);   non static  variable admin can't be reference from static context
	//	System.out.println(x);//we can't access or declare the non static variables in static context
	        System.out.println(y); //we can access but cant declare because of the (Sequence)
	}    

	
	{
		System.out.println(x);
		System.out.println(admin);
	}

	Demo(){
		System.out.println(x);
		System.out.println(y);
		System.out.println(admin);
	}
}
class Client{


	private static void fun(){
		System.out.println("we cant access private variables/methodes out of the class ");
	}

	public static void main(String[] args){
		Demo obj1=new Demo();
		fun();
	}
}
