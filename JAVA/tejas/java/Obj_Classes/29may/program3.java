class Demo{

	int x=10;

	Demo(String name){
	
		System.out.println(name);
	}

	Demo(){                                                                                //Demo(Demo this)
	        this("TJ7");
		System.out.println("in no-argument constructor");
		System.out.println(x); 
		System.out.println(this);
	}
	Demo(int x){                                                                         //Demo(Demo this,int x)

		this();
		System.out.println("in parameterised constructor");
		System.out.println(this);
//		this();                                                                    //call to this() must be first statement in the constructor
		System.out.println(this.x); 
		System.out.println(x);                                                    //(shows nearest x hence no confussion)
	}


	public static void main(String[] args){

		Demo obj1=new Demo(50);                                                 //Demo(obj1,10)
		System.out.println(obj1);
	}                                            
}

/* class Client{

	static{
		Demo obj1=new Demo(50);
	}

	public static void main(String[] args){
		//Demo obj2=new Demo();
	}
}*/


