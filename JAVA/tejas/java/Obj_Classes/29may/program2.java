class Demo{

	int x=10;

	Demo(){
		System.out.println("in constructor1");
	}

	Demo(int x){
		System.out.println("in constructor2");
	}

	public static void main(String[] args){
		Demo obj1=new Demo();
		Demo obj2=new Demo(10);
	}
}
/* note: jari bytecode madhe bipush 10 both constructors madhe jari dakhavat aasel tri jya constructor chi stack frame push hoil tyach constructor madhe 10 push hoil (check the digram for better understanding )*/	
