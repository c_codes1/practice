class Demo{

	static int x=10;

	static{
	
		static int y=20;   //error illigal start of operation
	}

	static void fun(){
	
		static int z=30;    //error illigal start of operation
	}

	void gun(){
	
		static int 40;   //error illigal start of operation
	}
}
