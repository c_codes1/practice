class Demo{

	int x=10;

	Demo(){
		System.out.println("constructor");
	}

	{
		
		System.out.println("in instance block1");
	}

	void fun(){
		System.out.println(this);
	}
	void gun(){
		fun();
	}

	public static void main(String[] args){
		Demo obj=new Demo();
		System.out.println("Main");
		obj.gun();
		System.out.println(obj);

	}

	{
		System.out.println("in instance block2");
	}
}
