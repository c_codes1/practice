class Demo{

	int x=10;
	static int y=20;
	 
	Demo(){
		 System.out.println("In Constructor");
        }

	static{
		System.out.println("In static block1");
	}

	{
		System.out.println("in instance block1");
	}

	public static void main(String[] args){

		System.out.println("main");
		Demo obj=new Demo();
	}
	
	static{
		System.out.println("in static block2");
	}
	{
		System.out.println("in instance block2");
	}
}


