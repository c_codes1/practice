

class Demo{

	int x=10;
	
	Demo(){				//Demo(Demo this)
		System.out.println("In Constructor");
		System.out.println(this);
		System.out.println(x);
		System.out.println(this.x);
	}	

	void fun(){			//fun(Demo this)

		System.out.println(this);
		System.out.println(x);
	}
/*	static void gun(){

		System.out.println(this);
	
	
	} */                // error: non static variable this can't  be referenced from static context

	public static void main(String args[]){

		Demo obj=new Demo();	//Demo(obj)
		System.out.println(obj);
		obj.fun(); 		//fun(obj)
//		gun();
	}
	//note: this reference is only used in non-static methodes and Constructor


}


