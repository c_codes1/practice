

class Demo{

	boolean x=true;
	Demo(){

		System.out.println("No argument constructor");
	}

	Demo(int x){

		System.out.println("Constructor with argument");
		System.out.println(x);
	}
	Demo(Demo xyz){
		System.out.println("Constructor with Object argument");
		System.out.println("shall we access the object from constructor:"+xyz.x);
		
	}

	public static void main(String[] ar){

		Demo obj1=new Demo();
		Demo obj2=new Demo(10);
		Demo obj3=new Demo(obj1);
	}
}

