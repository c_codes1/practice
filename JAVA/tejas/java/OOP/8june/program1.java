// Access specifier in Overriding:


class Parent{

	public void fun(){

		System.out.println("Parent Fun");
	}
}
class Child extends Parent{

	void fun(){ //error:  attempting to assign weaker access privileges; was public

		System.out.println("In child fun");
	}
}




































