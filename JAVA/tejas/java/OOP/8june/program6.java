class Parent{

	static void fun(){
		
		System.out.println("Parent fun");
	}
}
class Child extends Parent{

	void fun(){  //error:  overridden method is static

		System.out.println("Child fun");
	}
}
