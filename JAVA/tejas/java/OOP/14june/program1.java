interface Demo{

    int x=10;
    void fun();
    
}
class asach{
    static int y=20;
    
}
//bytecode(Demo.class)
/* interface Demo {
  public static final int x;

  public abstract void fun();
}  */

//bytecode(asach.class)
/*
class asach {
  static int y;

  asach();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: return

  static {};
    Code:
       0: bipush        20
       2: putstatic     #7                  // Field y:I
       5: return
} */

//note: hence prove interface madhe static block nasato x la stack frame vr jaga milate 
