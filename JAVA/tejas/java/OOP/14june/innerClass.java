class outer{

	class inner{

		void m1(){

			System.out.println("In m1-inner class");
		}
	}
	void m2(){

		System.out.println("In m2-inner class");
	}
}
class Client{

	public static void main(String...args){

		outer obj=new outer();
		obj.m2();

		outer.inner obj1=obj.new inner(); 
//		outer.inner obj1=new outer().new inner();
		obj1.m1();
	}
}
