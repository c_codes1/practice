class Parent{

	Parent(){

		System.out.println("In Parent Constructor");
	}
	void property(){

		System.out.println("Home,car,Gold");
	}
	void marry(){

		System.out.println("Dipika Padukone");
	}
}
class Child extends Parent{

	Child(){
		System.out.println("In Child Constructor");
	}
	void marry(){

		System.out.println("Alia Bhatt");
	}
}
class Client {

	public static void main(String[] args){

		Child obj=new Child();
		obj.marry();
	}
}
