class Parent{

        private void fun(){

                System.out.println("Parent Fun");
        }
}
class Child extends Parent{

        void fun(){ //error:  attempting to assign weaker access privileges; was package

                System.out.println("In child fun");
        }
}
class Client{


        public static void main(String[] TJ7){

//              Parent obj1=new Child();  error: Private access in Parent class
//              obj1.fun();
//              Object obj2=new Child();
//		obj2.fun();
		Child obj3=new Child();
		obj3.fun();

        }
}
//note:this is note a scenerio of overriding: here two different methodes are there
