class Parent{

        void fun(){

                System.out.println("Parent Fun");
        }
}
class Child extends Parent{

        private void fun(){ //error:  attempting to assign weaker access privileges; was package

                System.out.println("In child fun");
        }
}
class Client{


        public static void main(String[] TJ7){

                Parent obj=new Child();
                obj.fun();

        }
}
