class Parent{

        void fun(){

                System.out.println("Parent fun");
        }
}
class Child extends Parent{

        static void fun(){  //error:  overriding method is static

                System.out.println("Child fun");
        }
}
