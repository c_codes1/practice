class Parent{

	char fun(){

		System.out.println("Parent fun");
                return 'A';
	}
}
class Child extends Parent{
	
	
	int fun(){

		System.out.println("Parent fun");
                return 10;
	}
}
class Client{

	public static void main(String[] args){

		Parent obj1=new Child();
		obj1.fun(); // error: fun() in Child cannot override fun() in Parent

/*		Child obj2=new Child();
		obj2.fun(); // error: fun() in Child cannot override fun() in Parent  */

	}
}
//note:if return type is premitive then they should have same return types
//note: if the return type is Covarient then it is allowed in overriding
