class Demo{

	void fun(Object obj){

		System.out.println("Object");
	}

	void fun(String str){

		System.out.println("String");
	}
}
class Client{

	public static void main(String[] args){

		Demo obj=new Demo();
		obj.fun("Core2web");
		obj.fun(new StringBuffer("Core2web"));
		obj.fun(null);// note:Parent always gives priority to his child
		obj.fun(10);
		obj.fun(10.5);
		obj.fun(10.5f);
		obj.fun(obj);
	}
}


