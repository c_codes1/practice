interface Demo{

    void fun();
    default void gun(){
        System.out.println("In Gun");
    }
    static void run(){
        System.out.println("In run");
    }
}
class DemoChild implements Demo{

    public void fun(){
        System.out.println("In fun");
    }

}
class Client{

    public static void main(String[] args) {
        Demo obj=new DemoChild();
        obj.fun();
        obj.gun();
        Demo.run();
    }
}
    
    


