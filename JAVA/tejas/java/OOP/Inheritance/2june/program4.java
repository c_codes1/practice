class Parent{

	int x=10;
	static int y=20;

	static{
		System.out.println("parent static block");
	}
	Parent(){
		orebj1.method1();
		System.out.println("In Parent Constructor");
	}
	void method1(){
		System.out.println(x);
		System.out.println(y);
	}

	static void method2(){
		System.out.println(y);
	}
}
class Child extends Parent{

	static{
		System.out.println("In child static block");
	}
	Child(){
		System.out.println("In child constructor");
	}
}
class Client{

	public static void main(String[] args){
		Child obj1=new Child();
		obj1.method1();
		obj1.method2();
	}
}
