class Parent{

        Parent(){

		System.out.println(this);
                System.out.println("In parent Constructor");
        }
        void parentProperty(){
                System.out.println("Flat,Car,GOLD");
        }
}
class Child extends Parent{

        Child(){
		System.out.println(this);
                System.out.println("In Child Constructor");
        }
}
class Client{

        public static void main(String[] args){

                Parent obj1=new Child();
//    	        Child obj2=new Parent();   error: incompatible types: Parent cannot be converted to Child
                obj1.parentProperty();
        }
}
