class Parent{

        Parent(){

                System.out.println("In parent Constructor");
                System.out.println(this);
        }
        void parentProperty(){
                System.out.println("Flat,Car,GOLD");
        }
}
class Child extends Parent{

        Child(){

                System.out.println("In Child Constructor");
                System.out.println(this); // if all three address are same then only child class cha object ahe heap vr dusara gola banala nahi parent sathi
        }
}
class Client{

        public static void main(String[] args){

                Child obj=new Child();
                System.out.println(obj);
                obj.parentProperty();
        }//next code madhe neat clear hoil ki child cha this parent la jato te
}
