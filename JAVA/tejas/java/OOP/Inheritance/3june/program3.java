// this is an Polymoephism :Overloading:

class Demo{

	void fun(int x){
		System.out.println(x);
	}
	void fun(float y){
		System.out.println(y);
	}
	void fun(Demo obj){
		System.out.println("In Demo Parameter");
		System.out.println(obj);
	}
	public static void main(String[] ar){

		Demo obj=new Demo();
		obj.fun(10);
		obj.fun(10.5f);
		obj.fun(obj);
	}
}
