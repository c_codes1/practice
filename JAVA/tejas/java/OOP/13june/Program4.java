interface Demo1{

    default void fun(){
        System.out.println("in fun Demo1");
    }
    
}
interface Demo2{
    default void fun(){
        System.out.println("in fun Demo2");
    }
}
class DemoChild implements Demo1,Demo2{

	//note: use when you have to override method ,it will run if you override fun method 

}
class Client{

    public static void main(String[] args) {
        
        DemoChild obj=new DemoChild();
        obj.fun();
        obj.gun();
    }
}

/* error: types Demo1 and Demo2 are incompatible;
class DemoChild implements Demo1,Demo2{
^
  class DemoChild inherits unrelated defaults for fun() from types Demo1 and Demo2
Program4.java:23: error: cannot find symbol
        obj.gun();                           */
