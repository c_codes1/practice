interface Demo{

    static void fun(){  //public static void fun()
        System.out.println("In fun-Demo");
    }
    default void gun(){ // public default void gun()    note:here default is not a access Spacefier, default is a keyword /modifier
        System.out.println("In gun-Demo");
    }
    
}
