interface Demo{

        void gun();
        
        default void fun(){

            System.out.println("in fun -Demo");
        }
}
class DemoChild implements Demo{

     void gun(){  //attempting to assign weaker access privileges; was public

        System.out.println("in fun-DemoChild");
     }
}

/*note: all methodes which are declared in interface are byDefault public (access Specifier), but note that this second method in interface Demo has note default specifier it is modifier or we can call it keyword which is used to make concrit method in interface.(from 1.8) version */
