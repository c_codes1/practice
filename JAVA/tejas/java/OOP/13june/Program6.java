interface Demo{

    static void fun(){

        System.out.println("In fun");
    }
    
}
class DemoChild implements Demo{

}
class Client{

    public static void main(String[] args) {
        
        Demo obj=new DemoChild();
        obj.fun();
    }
    //note: static in inferface,can not be inherites(khali yet nahi fun() method ,class madhe yete pn overeride karu shakat nahi)
    //if we have to access the static method in interface we have to access by referance(name of the Interface) like Demo.fun();
}
/* error: illegal static interface method call
        obj.fun();
               ^
  the receiver expression should be replaced with the type qualifier 'Demo'     */
