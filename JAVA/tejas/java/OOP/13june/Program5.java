interface Demo1{

    default void fun(){
        System.out.println("in fun Demo1");
    }
    
}
interface Demo2{
    default void fun(){
        System.out.println("in fun Demo2");
    }
}
class DemoChild1 implements Demo1,Demo2{

    public void fun(){

        System.out.println("in fun-DemoChild1");
    }


}

class DemoChild2 implements Demo1,Demo2{

    public void fun(){

        System.out.println("in fun-DemoChild2");
    }


}
class Client{

    public static void main(String[] args) {
        
        DemoChild1 obj1=new DemoChild1();
        obj1.fun();
        
	DemoChild2 obj2=new DemoChild2();
        obj2.fun();
	
	Demo1 obj3=new DemoChild2();
        obj3.fun();
	
	Demo2 obj4=new DemoChild2();
        obj4.fun();
    }
}

//note : we can overeride if also their is ambegious senerio it will shows always class method(DemoChild chi fun method) 
