import java.io.*;
abstract class Product{

    private String name;
    private double price;

    Product(String name,double price){
        this.name=name;
        this.price=price;
    }

    String pName(){
        return "Product name:"+name;
    }
    double pPrice(){
        return price;
    }
    abstract void productDescription();
}
class Clothes extends Product{

    private String size;

    Clothes(String name,double price,String size){
        super(name,price);
        this.size=size;
    }
    void productDescription(){
        System.out.println("Size:"+size);
    }
}
class electronicGadget extends Product{
    private String Brand;
    electronicGadget(String name,double price,String Brand){
        super(name,price);
        this.Brand=Brand;
    }
        void productDescription(){
        System.out.println("Brand:"+Brand);
    }
    
}
class Customer{

    public static void main(String[] args)throws IOException{
        
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter size of shirt :");
        String size=br.readLine();
        Product shirt=new Clothes("Shirt",1200,size);
        System.out.println(shirt.pName());
        System.out.println("Price:"+shirt.pPrice());
        shirt.productDescription();
        
        System.out.println("Enter Brand name:");
        String Brand=br.readLine();
        Product Gadget=new electronicGadget("mobile",60000,Brand);
        System.out.println(Gadget.pName());
        System.out.println("Product price:"+Gadget.pPrice());
        Gadget.productDescription();    

    }
}
