class Singlton{

	static Singlton obj=new Singlton();

	private Singlton(){

	}

	static Singlton getObject(){

		return obj;
	}
}
class Client{

	public static void main(String[] args){

		Singlton obj1=Singlton.getObject();
		System.out.println(obj1);

		Singlton obj2=Singlton.getObject();
		System.out.println(obj2);

		
		Singlton obj3=Singlton.getObject();
		System.out.println(obj3);
	}
}
