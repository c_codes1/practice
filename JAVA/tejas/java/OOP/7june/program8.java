class Parent{

	String fun(){

		System.out.println("In parent fun");
		return new String();
	}
}
class Child extends Parent{


	Object fun(){

		System.out.println("In Child fun");
		return new Object();
	}
}
/*It's important to note that the return type of the overridden method in the child class can be a subtype of the return type in the parent class, but it cannot be a supertype. In this case, Object is a supertype of String, so the code will compile successfully. However, if you tried to return a String in the Child class, it would be considered a compilation error. */
