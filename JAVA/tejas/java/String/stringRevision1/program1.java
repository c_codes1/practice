class StringDemo{


	public static void main(String[] ar){

		String str1="Core2web";
		String str2=new String("Core2web");
		char [] str3={'C','o','r','e','2','w','e','b'};

		System.out.println(str1);
		System.out.println(System.identityHashCode(str1));
		
		System.out.println(str2);
		System.out.println(System.identityHashCode(str2));
		
		System.out.println(str3);
		System.out.println(System.identityHashCode(str3));
	}
}
