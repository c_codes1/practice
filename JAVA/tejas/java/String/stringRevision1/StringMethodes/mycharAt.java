import java.io.*;
class MycharAt{

	static char mycharAt(String str,int x){

		char []arr=str.toCharArray();

		return arr[x];
	}

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a String:");
		String str1=br.readLine();
		System.out.println("Enter a index");
		int index=Integer.parseInt(br.readLine());

		System.out.println(mycharAt(str1,index));
	}

}
