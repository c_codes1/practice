import java.io.*;

class CompareToIgnoreCase {
    int compareToIgnoreCase(String str1, String str2) {
        int len1 = str1.length();
        int len2 = str2.length();
        int minLength = len1 < len2 ? len1 : len2;

        for (int i = 0; i < minLength; i++) {
            char ch1 = str1.charAt(i);
            char ch2 = str2.charAt(i);

            if (ch1 != ch2) {
                // Convert characters to lowercase if they are uppercase
                if (ch1 >= 'A' && ch1 <= 'Z') {
                    ch1 = (char) (ch1 + 32);
                }
                if (ch2 >= 'A' && ch2 <= 'Z') {
                    ch2 = (char) (ch2 + 32);
                }

                if (ch1 != ch2) {
                    return ch1 - ch2;
                }
            }
        }

        return len1 - len2;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter string1:");
        String str1 = br.readLine();

        System.out.println("Enter string2:");
        String str2 = br.readLine();

        CompareToIgnoreCase obj = new CompareToIgnoreCase();
        System.out.println(obj.compareToIgnoreCase(str1, str2));

        
    }
}
