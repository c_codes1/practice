/*

1
2 c
4 e 6
7 h 9 j   */


class core2web{

	public static void main(String[] a){

		int var1=1;
		int n=4;
		char var2='a';

		for(int i=1; i<=n; i++){
			for(int j=1; j<=i; j++){
				if(j%2==1){
					System.out.print(var1+" ");
				}else{
					System.out.print(var2+" ");
				}
				var1++;
				var2++;
			}
			System.out.println();
		}
	}
}
