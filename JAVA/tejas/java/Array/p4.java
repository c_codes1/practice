import java.io.*;

class arrDemo{

	public static void main(String[] ar){

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

//		int arr1[]=new int[4]{10,20,30,40};
		/*array creation with both dimension expression and initialization is illegal
                int arr1[]=new int[4]{10,20,30,40};  
                                     ^                       */

		int arr1[]={10,20,30,40,50};
		char arr2[]={'A','B','C','D'};
		float arr3[]={10.5f,20.6f};
		boolean arr4[]={true,false,true};

		for(int i=0; i<5; i++){
			System.out.print(arr1[i]+" ");
		}
		System.out.print("\n");
		for(int i=0; i<4; i++){
			System.out.print(arr2[i]+" ");
		}
		System.out.print("\n");
		for(int i=0; i<2; i++){
			System.out.print(arr3[i]+" ");
		}
		System.out.print("\n");
		for(int i=0; i<3; i++){
			System.out.print(arr4[i]+" ");
		}

		System.out.print(arr3[2]);

	


	}
}
