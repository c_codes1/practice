

class TJ7{

	public static void main(String[] args){


		int arr[][]=new int[3][3];

		arr[0][0]=10;
		arr[0][1]=20;
		arr[0][2]=30;
		arr[1][0]=40;
		arr[1][1]=50;
		arr[1][2]=60;
		arr[2][0]=70;
		arr[2][1]=80;
		arr[2][2]=90;

		System.out.println(arr[1][1]);
		System.out.println(arr[0]);// this is the proof that new object of 1D array is created
		System.out.println(arr[1]);
		System.out.println(arr[2]);
		System.out.println(arr);
	}
}

/*
 o/p:

[I@7ad041f3 ->0 ([)
[I@251a69d7 ->1
[I@7344699f ->2
[[I@6b95977 ->and this whole 2D array's address/reference([[)    */
