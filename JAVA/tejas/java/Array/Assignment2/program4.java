
import java.io.*;
class program4 {

    int search(int arr[],int x){

        for(int i=0; i<arr.length;i++){

            if(arr[i]==x){
                return i;
            }
        }
        System.out.println("Element Not found: ");
        return -1;
    }
    public static void main(String[] args)throws IOException {
        
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter a size of an array:");
        int size=Integer.parseInt(br.readLine());
        int[]arr=new int[size];
        System.out.println("Enter a array elements:");
        for(int i=0; i<arr.length;i++){

            arr[i]=Integer.parseInt(br.readLine());

        }
        program4 obj1=new program4();
        System.out.println("enter a number to search:");
        int ele=Integer.parseInt(br.readLine());
        int ret=obj1.search(arr,ele);
        if(ret>=0){
            System.out.println("number found at index:"+ret);
        }

    }
    
}
