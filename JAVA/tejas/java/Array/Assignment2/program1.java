//Write a program to create an Array of 'n' integer elements,When n value should be taken from the user.insert the values from the use and find the sum of all elements in array
import java.io.*;
class Sumofarray_ele{



	int sum(int arr[]){

		int ret=0;

		for(int i=0; i<arr.length;i++){

			ret=ret+arr[i];
		}
		return ret;
	}
}
class Client{


	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a size of array:");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("Enter elements in array:");
		for(int i=0; i<arr.length; i++){

			arr[i]=Integer.parseInt(br.readLine());
		}

		Sumofarray_ele obj=new Sumofarray_ele();
		System.out.println("sum of array elements is="+obj.sum(arr));
	}
}


