//WAP to find common element between two array:

import java.io.*;
public class program8{

    void commonEle(int arr1[],int arr2[],int size){

        int []uncommon=new int[size+size];
        int c=0;
        

        for(int i=0; i<arr1.length; i++){
            int count=0;
            for(int j=0;j<arr2.length;j++){

                if(arr1[i]==arr2[j]){
                    count++;
                }
                
            }
            if(count==0){
                    
                    uncommon[c]=arr1[i];
                    c++;
                }
        }
        for(int i=0; i<arr2.length; i++){
            int count=0;
            for(int j=0;j<arr1.length;j++){

                if(arr2[i]==arr1[j]){
                    count++;
                }
                
            }
            if(count==0){
                    
                    uncommon[c]=arr2[i];
                    c++;
                }
        }
	System.out.println("uncommon elements :");
           for(int i=0; i<uncommon.length;i++){
            int count=0;
            for(int j=0; j<uncommon.length;j++){
                
                if(uncommon[i]==uncommon[j]){
                    count++;
                }
            }
            if(count==1){
                System.out.println(uncommon[i]);
            }
        }

    }

    public static void main(String[] args)throws IOException {

        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter size of array:");
        int size=Integer.parseInt(br.readLine());
        int arr1[]=new int[size];
        int arr2[]=new int[size];
        System.out.println("Enter a array elements for array1:");
        for(int i=0; i<arr1.length;i++){
            arr1[i]=Integer.parseInt(br.readLine());
        }
        System.out.println("Enter a array elements for array2:");
        for(int i=0; i<arr2.length;i++){
            arr2[i]=Integer.parseInt(br.readLine());
        }
        program8 obj=new program8();

        obj.commonEle(arr1,arr2,size);
    
    }
    
}
