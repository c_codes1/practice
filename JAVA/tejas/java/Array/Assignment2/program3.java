//WAP to to find the sum of even and odd numbers in an array:
import java.io.*;
class ArrayDemo{
    
    void evenODDsum(int arr[]){
        int evenSum=0;
        int oddSum=0;
        for(int i=0; i<arr.length;i++){

            if(arr[i]%2==0){
                evenSum=evenSum+arr[i];
            }else{
                oddSum=oddSum+arr[i];
            }
        }
        System.out.println("Sum of even numbers: "+evenSum);
        System.out.println("Sum of odd numbers: "+oddSum);
        
    }
    public static void main(String[] args)throws IOException {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter a size of array:");
        int size=Integer.parseInt(br.readLine());
        int arr[]=new int[size];
        System.out.println("Enter a array elements");
        for(int i=0; i<arr.length;i++){

            arr[i]=Integer.parseInt(br.readLine());
        }
        ArrayDemo obj=new ArrayDemo();
        obj.evenODDsum(arr);    

    }
}