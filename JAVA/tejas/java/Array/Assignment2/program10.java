//WAP to print the elements whose addition of digits is even:
import java.io.*;
public class program10{

    void sumOfDigit(int[]arr){

	    System.out.println("OUTPUT:-");
        int sum=0;
        for(int i=0; i<arr.length;i++){

            
            int temp=arr[i];

            while(temp!=0){
                int rem=0;
                rem=temp%10;
                sum=sum+rem;
                temp=temp/10;
            }
            if(sum%2==0){
                System.out.println(arr[i]);
            }
	    sum=0;

            
        }
    }
    public static void main(String[] args)throws IOException{

        BufferedReader br=new BufferedReader (new InputStreamReader(System.in));
        System.out.println("Enter a size of array:");
        int size=Integer.parseInt(br.readLine());
        System.out.println("Enter elements:");
        int arr[]=new int[size];
        for(int i=0; i<arr.length;i++){

            arr[i]=Integer.parseInt(br.readLine());
        }
        program10 obj=new program10();
        obj.sumOfDigit(arr);

        

    }
    
}

