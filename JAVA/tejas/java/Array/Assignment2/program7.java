//WAP to find common element between two array:

import java.io.*;
public class program7 {

    void commonEle(int arr1[],int arr2[],int size){

        int []common=new int[size];

        for(int i=0; i<arr1.length; i++){
            for(int j=0;j<arr2.length;j++){

                if(arr1[i]==arr2[j]){
                    common[i]=arr2[j];
                    break;

                }    
            }
        }
        System.out.println("enter common elements:");
        for(int i=0; i<common.length;i++){
            int count=0;
            for(int j=0; j<common.length;j++){
                
                if(common[i]==common[j]){
                    count++;
                }
            }
            if(count==1){
                System.out.println(common[i]);
            }
        }
    }

    public static void main(String[] args)throws IOException {

        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter size of array:");
        int size=Integer.parseInt(br.readLine());
        int arr1[]=new int[size];
        int arr2[]=new int[size];
        System.out.println("Enter a array elements for array1:");
        for(int i=0; i<arr1.length;i++){
            arr1[i]=Integer.parseInt(br.readLine());
        }
        System.out.println("Enter a array elements for array2:");
        for(int i=0; i<arr2.length;i++){
            arr2[i]=Integer.parseInt(br.readLine());
        }
        program7 obj=new program7();

        obj.commonEle(arr1,arr2,size);
    }
}
