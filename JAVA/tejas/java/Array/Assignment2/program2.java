//WAP to find the number of even and odd integers in  a given array of integers:

import java.io.*;
class ArrayDemo{


	void count(int arr[]){

		int evenCount=0;
		int oddCount=0;

		for(int i=0; i<arr.length;i++){

			if(arr[i]%2!=0){
				oddCount++;
			}else{
				evenCount++;
			}
		}
		System.out.println("Number of Even elements:"+evenCount);
		System.out.println("Number of Odd elements:"+oddCount);
	}
	
	public static void main(String[] TJ7)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

			System.out.println("Enter size of Array:");
			int size=Integer.parseInt(br.readLine());
			int[] arr=new int[size];

			System.out.println("Enter a array elements:");

			for(int i=0; i<arr.length; i++){

				arr[i]=Integer.parseInt(br.readLine());
			}
                        
                        ArrayDemo obj=new ArrayDemo();
			obj.count(arr);

		}
}




