import java.io.*;
import java.util.*;

class TJ7{

	public static void main(String[] ar)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a size of array:");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		System.out.println("Enter a array elements:");
		int product=1;
		for(int i=0; i<size; i++){

			arr[i]=Integer.parseInt(br.readLine());

			if(arr[i]%2==0){
				product=product*arr[i];
			}
		}
		System.out.println("Product of all even elements:"+product);
	}
}

