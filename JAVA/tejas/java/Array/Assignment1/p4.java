import java.io.*;


class TJ7{

	public static void main(String[] a)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a Size of a array:");
		int size=Integer.parseInt(br.readLine());

		char arr[]=new char[size];

		System.out.println("Enter a array elements:");

		for(int i=0; i<size; i++){
			
			arr[i]=(char)br.readLine().charAt(0);
		}
		
		for(int i=0; i<size; i++){

			if(arr[i]=='a'||arr[i]=='e'||arr[i]=='i'||arr[i]=='o'|| arr[i]=='u'){
				System.out.print(arr[i]+" ");
			}
		}
	}
}

