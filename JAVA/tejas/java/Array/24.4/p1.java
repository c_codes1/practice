class ArrayDemo{

	public static void main(String[] ar){
		

		int arr1[]={100,200,300,400};
		short arr2[]={10,20,30,40};
		byte arr3[]={1,2,3,4};
		boolean arr4[]={true,false,true};
		char arr5[]={'T','E','J','A','S'};
		float arr6[]={10.5f,20.5f,30.5f,40.5f};

		System.out.println(arr1);
		System.out.println(arr2);
		System.out.println(arr3);
		System.out.println(arr4);
		System.out.println(arr5);
		System.out.println(arr6);
	}
}
