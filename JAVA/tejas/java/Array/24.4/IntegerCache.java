

class IntegerCacheDemo{

	public static void main(String[] args){

		int x=10;
		int y=10;
		Integer z=10;
		Integer a= new Integer(10); // different because it creates new object
//	        Integer a=Integer.valueOf(10);  same identity HashCode 


		System.out.println(System.identityHashCode(x));
		System.out.println(System.identityHashCode(y));
		System.out.println(System.identityHashCode(z));
		System.out.println(System.identityHashCode(a));
	}
}
