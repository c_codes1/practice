//WAP to find a palindrome number from an array and return its index.
import java.io.*;
class palindromeArray{

    void Palindrome(int[]arr){

        for(int i=0;i<arr.length;i++){
            int temp=arr[i];
            int rev=0;
            while(temp!=0){
                int rem=temp%10;
                rev=rev*10+rem;
                temp=temp/10;

            }
             if(arr[i]==rev){
                System.out.println("Palindrome no "+arr[i]+" found at index: "+i);
             }
        }
    }

    public static void main(String[] args)throws IOException {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter a array size:");
        int size=Integer.parseInt(br.readLine());
        int[] arr=new int[size];
        System.out.println("Enter array elements:");

        for(int i=0;i<arr.length;i++){
            arr[i]=Integer.parseInt(br.readLine());
        }
        palindromeArray obj=new palindromeArray();
        obj.Palindrome(arr);
    
    }
    
}

