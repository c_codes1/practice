//WAP to print count of digits in elements of array:

import java.io.*;
class program1{

    void digitCount(int[] arr){
        System.out.println("OUTPUT:");
        for(int i=0;i<arr.length;i++){
            int temp=arr[i];
            int count=0;

            while(temp!=0){
                temp=temp/10;
                count++;
            }
            System.out.print(count+" ");
	    System.out.print("\n");

        }
    }

    public static void main(String[] args)throws IOException {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter size of an array:");
        int size=Integer.parseInt(br.readLine());
        int arr[]=new int[size];
        System.out.println("Enter array elements:");
        for(int i=0;i<arr.length;i++){

            arr[i]=Integer.parseInt(br.readLine());
        }
        program1 obj=new program1();
        obj.digitCount(arr);
        


    }
}
