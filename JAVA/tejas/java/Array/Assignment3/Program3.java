//WAP to find a Composite number from an array and return its index:
import java.io.*;
public class Program3 {

    void Composite(int arr[]){

        System.out.println("OUTPUT:");

        for(int i=0;i<arr.length;i++){
            int count=0;
            for(int j=1;j<=arr[i]; j++){
                
                if(arr[i]%j==0){
                    count++;
                }
            }
            if(count>=2){
                System.out.println("composite "+arr[i]+" found at index:"+i);
            }
            
        }
    }


    public static void main(String[] args)throws IOException {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("enter size of array:");
        int size=Integer.parseInt(br.readLine());
        int []arr=new int[size];
        System.out.println("Enter array elements:");
        for(int i=0;i<arr.length;i++){

            arr[i]=Integer.parseInt(br.readLine());

        }
        Program3 obj=new Program3();
        obj.Composite(arr);
    }
    
}
