//WAP to find a strong number from an array and returns its index:

import java.io.*;
class strongArray{

    void Strong(int[]arr){
	    
	    int flag=0;
        
        for(int i=0;i<arr.length;i++){
            int sum=0;
            int num=arr[i];
            int temp=num;
        
            while(temp!=0){
                int rem=temp%10;
                int fact=1;
                for(int j=1; j<=rem;j++){
                    fact=fact*j;
                }
                temp=temp/10;
                sum=sum+fact;
            }
        
            if(sum==num){
		flag=1;
                System.out.println("Strong no "+arr[i]+" found at index: "+i);
            }

        }
	if(flag==0){
		System.out.println("Strong number not found");
	}
    }


    public static void main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter size of array:");
        int size=Integer.parseInt(br.readLine());
        int arr[]=new int[size];
        System.out.println("Enter array elements:");
        for(int i=0; i<arr.length;i++){

            arr[i]=Integer.parseInt(br.readLine());
        }
        strongArray obj=new strongArray();
        obj.Strong(arr);

    }
    
}

