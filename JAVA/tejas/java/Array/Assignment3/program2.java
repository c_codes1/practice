//WAP to reverse each element in an array:
import java.io.*;
public class program2 {

    void reverseEle(int[]arr){

        System.out.println("OUTPUT:");

        for(int i=0; i<arr.length;i++){

            int temp=arr[i];
            int rev=0;
            while(temp!=0){
                int rem=temp%10;
                rev=rev*10+rem;
                temp=temp/10;
            }
            System.out.println(String.format("%002d",rev));
        }

    }
    public static void main(String[] args)throws IOException{
        
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter array Size:");
        int size=Integer.parseInt(br.readLine());
        int []arr=new int[size];
        System.out.println("Enter Elements:");
        for(int i=0; i<arr.length;i++){
            arr[i]=Integer.parseInt(br.readLine());
        }
        program2 obj=new program2();
        obj.reverseEle(arr);
    }

}

