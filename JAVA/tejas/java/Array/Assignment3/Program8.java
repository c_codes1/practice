//WAP to find a Armstrong number from an array and returns its index:

import java.io.*;
class ArmstrongArray{

    void armStrong(int[]arr){
        
        for(int i=0;i<arr.length;i++){
            int sum=0;
            int num=arr[i];
            int temp=num;
        
            while(temp!=0){
                int rem=temp%10;
                int mult=1;
                for(int j=1; j<=3;j++){
                    mult=mult*rem;
                }
                temp=temp/10;
                sum=sum+mult;
            }
        
            if(sum==num){
                System.out.println("ArmStrong no "+arr[i]+" found at index: "+i);
            }
        }
    }


    public static void main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter size of array:");
        int size=Integer.parseInt(br.readLine());
        int arr[]=new int[size];
        System.out.println("Enter array elements:");
        for(int i=0; i<arr.length;i++){

            arr[i]=Integer.parseInt(br.readLine());
        }
        ArmstrongArray obj=new ArmstrongArray();
        obj.armStrong(arr);

    }
    
}

