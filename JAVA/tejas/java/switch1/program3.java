/* Write a program in which user should enter two numbers, if both the numbers are posative multiply them and provide to switch case to verify the number is even or odd if user enter any negative number program should terminate saying "sorry negative numbers are not allowed"   */


import java.io.*;

class TJ7{

	public static void main(String[] args)throws IOException{

		InputStreamReader isr= new InputStreamReader(System.in);
		BufferedReader br= new BufferedReader(isr);

		System.out.println("Enter a  num1:");
		int num1=Integer.parseInt(br.readLine());

		System.out.println("Enter a  num2:");
		int num2=Integer.parseInt(br.readLine());


		if(num1>0 && num2>0){

			int mult=num1*num2;

			switch(mult%2){

				case 1:
					System.out.println(mult+" is odd");
					break;
				case 0:
					System.out.println(mult+" is even");
					break;
			}

			
		}else{
			System.out.println("Sorry negative numbers are not allowed");
		}
	}
}


		


