class triangle{

	public static void main(String[] ar){


int sideA = 4; // Example side A
int sideB = 4; // Example side B
int sideC = 4; // Example side C

if ((sideA + sideB > sideC) && (sideA + sideC > sideB) && (sideB + sideC > sideA)) {
    if (sideA == sideB && sideB == sideC) {
        System.out.println("Equilateral triangle"); // All sides are equal
    } else if (sideA == sideB || sideB == sideC || sideA == sideC) {
        System.out.println("Isosceles triangle"); // Two sides are equal
    } else {
        System.out.println("Scalene triangle"); // No sides are equal
    }
} else {
    System.out.println("Not a triangle"); // Invalid triangle
}
	}
}
