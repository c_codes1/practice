import java.io.*;
class mergeSort{

    static int[] Merge(int[]arr1,int[]arr2){
        int n1=arr1.length;
        int n2=arr2.length;
        int[]arr3=new int[n1+n2];


        int i=0,j=0,k=0;

        while(i<n1&&j<n2){

            if(arr1[i]<=arr2[j]){
                arr3[k++]=arr1[i++];
            }else{
                arr3[k++]=arr2[j++];

            }
        }

        while(i<n1){
            arr3[k++]=arr1[i++];
        }
        while(j<n2){
            arr3[k++]=arr2[j++];
        }
        return arr3;


    }

    public static void main(String[] args)throws IOException {
        BufferedReader br=new BufferedReader (new InputStreamReader(System.in));
        System.out.println("Enter Array size:");
        int size=Integer.parseInt(br.readLine());
        System.out.println("Array elements:");
        int[]arr1=new int[size];
        int[]arr2=new int[size];
 
        for(int i=0; i<arr1.length; i++){
            arr1[i]=Integer.parseInt(br.readLine());
        }
        System.out.println("Array2 elements:");
        for(int i=0;i<arr2.length;i++){
            arr2[i]=Integer.parseInt(br.readLine());
        }

        int[] newArr =Merge(arr1,arr2);
        for(int z:newArr){
            System.out.println(z);
        }
        
    }
}
