//1295. Find Numbers with Even Number of Digits
//https://leetcode.com/problems/find-numbers-with-even-number-of-digits/
 class EvenDigits{

    public static void main(String[] args) {
        int[]arr={12,345,2,6,7896};
        System.out.println(findNumbers(arr));
        
    }
    static int findNumbers(int[]nums){
        int count=0;
        for(int num:nums){

            if(even(num)){
                count++;
            }
        }
	return count;
    }
    static boolean even(int num){

        int numberOfDigit=digits(num);
   /*   if(numberOfDigit%2==0){
            return true;
        }
        return false;
        */
        return numberOfDigit%2==0;
    }
    static int digits(int num){

        int count=0;
         while(num>0){
            count++;
            num/=10;
         }
         return count;
    }
    
}
