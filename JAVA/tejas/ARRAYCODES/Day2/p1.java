
import java.util.*;
class JaggedArray {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter a size of rows in 2d array:");
        int row=sc.nextInt();
        int col;

        int[][]arr=new int[row][];


        for(int x=0; x<row; x++){
                    System.out.println("Enter a size of column:");
                    col=sc.nextInt();
                    arr[x]=new int[col];
                 for(int i=0; i<arr[x].length; i++){
                        arr[x][i]=sc.nextInt(); 
                    }

        }

        for(int[] x:arr){
            System.out.println(Arrays.toString(x));
        }
        
    }
    
}
