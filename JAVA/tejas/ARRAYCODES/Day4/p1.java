import java.util.*;
class solution{
	
	public static void main(String[] args){

        int arr[]={5,7,7,8,8,10};
        int target=8;
        int[]ans=searchRange(arr,target);
        System.out.println(Arrays.toString(ans));
    }
    public static int[] searchRange(int[] nums, int target) {

        int start=0;
        int end=nums.length-1;
    

        int arr[]={-1,-1};

        while(start<=end){

            int mid=start+(end-start)/2;

            if(nums[mid]>target){

                end=mid-1;

            }else if(nums[mid]<target){
                start=mid+1;
            }else{
                arr[0]=mid;
                end=mid-1;
                if(end==start){
                    arr[0]=start;
                    break;
                }
            }
        }
        int i;
	i=nums.length-1; 
        while(i >= 0 && target == nums[i]){

                          
                arr[1]=i;
		i--;

        }
        return arr;
    }
}
