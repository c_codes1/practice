import java.util.Scanner;
class FloorOfnumber {

    static int FloorOfnumber(int[]arr,int target){

        int start=0;
        int end=arr.length-1;

        while(start<=end){
            int mid=start+(end-start)/2;

            if(arr[mid]>target){
                end=mid-1;
            }else if(arr[mid]<target){
                start=mid+1;
            }else{
                return mid;
            }
        }
        return arr[end];
    }

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int []arr={5,7,8,10,13,17,18,20};
        System.out.println("Enter a number to find:");
        int target=sc.nextInt();
        System.out.println(FloorOfnumber(arr,target));
    }
    
}
