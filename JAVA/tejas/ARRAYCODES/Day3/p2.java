/*Order-agnostic binary search is a variation of the traditional binary search algorithm that can be used to search for a target element in a sorted array, regardless of whether the array is sorted in ascending or descending order*/
import java.util.Scanner;
class BinearySearch{

    static int BinearySearch(int[]arr,int num){

        int start=0;
        int end=arr.length-1;

        while(start<=end){
            int mid=start+(end-start)/2;

            if(arr[mid]==num){
                return mid;
            }
            if(arr[start]<=arr[arr.length-1]){

                if(arr[mid]>num){
                    end=mid-1;
                }else{
                    start=mid+1;
                }
            }else{
                if(arr[mid]<num){
                    end=mid-1;
                }else{
                    start=mid+1;
                }

            }
        }
	return -1;
    }

    public static void main(String[] args) {
        
        Scanner sc=new Scanner(System.in);
        int[]arr={90,80,70,60,50,40,30,20};
	System.out.println("Enter element to find:");
        int target=sc.nextInt();

        int index=BinearySearch(arr,target);
        if(index>=0){	
		System.out.println("elemet found at "+index);
	}else{
		System.out.println("Element not found!!!!!!");
	}	

    }
}
