// object initilization using malloc:

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct IPL{

	char SName[20];
	int tTeams;
	double prize;
};

void main(){
	struct IPL *ptr=(struct IPL*)malloc(sizeof (struct IPL));

	strcpy((*ptr).SName,"TATA");
	ptr->tTeams=8;
	(*ptr).prize=10.00;

	puts(ptr->SName);
	printf("%d\n",(*ptr).tTeams);
	printf("%lf\n",ptr->prize);
}
