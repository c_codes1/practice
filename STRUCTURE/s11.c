// Pointer to a structure(structure pointer).


#include<stdio.h>

struct movie{
	char mName[20];
	int count;
	float price;
}obj1={"Tumbad",5,1000.0};

void main(){

	struct movie *Sptr= &obj1;

	printf("%s\n",obj1.mName);
	printf("%d\n",obj1.count);
	printf("%f\n",obj1.price);

	printf("%s\n",Sptr->mName);
	printf("%d\n",(*Sptr).count);
	printf("%f\n",Sptr->price);
}
